import { Router } from "express";
import controller  from '../controllers/logic';
import emailController from '../controllers/emails';
import callController from '../controllers/calls';
import meetingController from '../controllers/meetings';
import notesController from '../controllers/notes';
// import notes from "../controllers/notes";

const router = Router();

router.get('/',controller.homePage);
router.get('/all-engagements', controller.getAllEngagements);

// Emails API
router.get('/emails/', emailController.Home);
router.get('/emails/all', emailController.getAllEmails);
router.post('/emails/post', emailController.postEmail);

// Calls API
router.get('/calls/', callController.Home);
router.get('/calls/all', callController.getAllCalls);
router.post('/calls/post', callController.postCalls);

// Meetings API
router.get('/meetings/',meetingController.Home);
router.get('/meetings/all', meetingController.getAllMeetings);

// Notes API\
router.get('/notes/hello', notesController.Home);
router.get('/notes/all', notesController.getAllNotes);
// router.get('/notes/read', notesController.readNotes);

module.exports = router;
