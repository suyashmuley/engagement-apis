import { Request, Response } from "express";
import axios from 'axios';

import * as dotenv from 'dotenv';
dotenv.config();

const baseUri: string | undefined = process.env.BASE_URI 
const API_KEY: string | undefined = process.env.API_KEY;

const Home = async (req:Request, res: Response) => {
    console.log(baseUri);
};

const getAllEmails = async (req:Request, res: Response) => {
  const getAllEmailsUri = `${baseUri}/objects/emails?properties=hs_email_status&properties=hs_email_subject&properties=hs_email_text&hapikey=${API_KEY}`;
  try {
      const resp = await axios.get(getAllEmailsUri);
      const data = resp.data.results;
      res.json(data);
  } catch (error) {
      console.log(error);
  }  
};

//Post an Email Need to do more
const postEmail = async (req:Request, res: Response) => {
    const data = {
        properties: {
            hs_email_direction: "EMAIL",
            hs_email_sender_email: "abc@email.com",
            hs_email_sender_firstname: "Francis",
            hs_email_sender_lastname: "Seller",
            hs_email_to_email: "suyash.muley@elastikteams.com",
            hs_email_to_firstname: "Brian",
            hs_email_to_lastname: "Buyer",
            hs_email_status: "SENT",
            hs_email_subject: "Let'\''s talk",
            hs_email_text: "Thanks for taking your interest let'\''s find a time to connect"
        }
    };
    const postEmailUri = `${baseUri}/objects/emails?hapikey=${API_KEY}`;
    const headers = {
       'content-type': 'application/json'
    };
    try {
        await axios.post(postEmailUri, data, {headers});
        res.redirect("back")
    } catch (error) {
        console.log(error)
    }
    
}

export default {Home, getAllEmails, postEmail};
