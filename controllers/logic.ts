import { Request, Response } from "express";
import axios from 'axios';

import * as dotenv from 'dotenv';
dotenv.config();

const baseUri: string = 'https://api.hubapi.com/engagements/v1/'
const API_KEY: string | undefined = process.env.API_KEY;

const homePage =async (req:Request, res:Response) => {
  res.send('Hello');
};

const getAllEngagements = async (req:Request, res: Response) => {
  const getAllEngagementsUri = `${baseUri}/engagements/paged?hapikey=${API_KEY}`;
  try {
      const resp = await axios.get(getAllEngagementsUri);
      const data = resp.data.results;
      res.json(data);
  } catch (error) {
      console.log(error);
  }  
};

export default {homePage, getAllEngagements};