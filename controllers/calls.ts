import { Request, Response } from "express";
import axios from 'axios';

import * as dotenv from 'dotenv';
dotenv.config();

const baseUri: string | undefined = process.env.BASE_URI 
const API_KEY: string | undefined = process.env.API_KEY;

const Home = async (req:Request, res: Response) => {
    console.log('Hello');
};


const getAllCalls = async (req:Request, res: Response) => {
  const getAllCallsUri: string = `${baseUri}/objects/calls?properties=hs_call_title&properties=hs_call_body&properties=hs_call_duration&hapikey=${API_KEY}`;
  try {
      const resp = await axios.get(getAllCallsUri);
      const data: any = resp.data.results;
      res.json(data);
  } catch (error) {
      console.log(error);
  }  
};


const postCalls = async (req:Request, res:Response) => {
    const data = {
        properties: {
            hs_timestamp: "2021-03-17T01:32:44.872Z",
            hs_call_title: " call",
            hubspot_owner_id: "11349275740",
            hs_call_body: " Decision maker out, will call back tomorrow",
            hs_call_duration: "3800",
            hs_call_from_number: "(857)Ï829 5489",
            hs_call_to_number: "(509) 999 9999",
            hs_call_recording_url: "https://api.twilio.com/2010-04-01/Accounts/AC890b8e6fbe0d989bb9158e26046a8dde/Recordings/RE3079ac919116b2d22",
            hs_call_status: "COMPLETED"
        }
    };
    const headers: {
        "Content-Type": string
    } = {
        "Content-Type": "application/json"
    };
    const postCallUri = `${baseUri}/objects/calls?hapikey=${API_KEY}`;
    try {
        await axios.post(postCallUri,data,{headers});
    } catch (error) {
        console.log(error)
    }
};


export default {Home, getAllCalls, postCalls};
