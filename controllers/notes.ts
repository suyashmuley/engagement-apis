
import { Request, Response } from "express";
import axios from 'axios';

import * as dotenv from 'dotenv';
dotenv.config();

const baseUri: string | undefined = process.env.BASE_URI 
const API_KEY: string | undefined = process.env.API_KEY;

const Home = async (req:Request, res: Response) => {
    console.log('Hello');
};

const getAllNotes = async (req:Request, res:Response) => {
    const getAllNotesUri: string = `${baseUri}/objects/notes?properties=hs_note_body&hapikey=${API_KEY}`;
    try {
        const resp = await axios.get(getAllNotesUri);
        const data: any = resp.data.results;
        res.json(data);
    } catch (error) {
        console.log(error)
    }
    
};

// const readNotes =async (req:Request, res:Response) => {
//     // const notesId: number = 19758201366;
//     const readNotesUri: string = `${baseUri}/objects/notes/19758201366?hapikey=${API_KEY}`;
//     try {
//         const resp = await axios.get(readNotesUri);
//         const data: any = resp.data.results;
//         res.json(data);
//     } catch (error) {
//         console.log(error);
//     }
    
// }

export default {Home, getAllNotes, };