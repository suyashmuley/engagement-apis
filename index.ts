import express from 'express';
const app: express.Application = express();
const routes = require('./routes/route');

const port: number = 5000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/', routes);

app.listen(port, ()=> console.log(`Listening on http://localhost:${port}`));

